# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/alicloud" {
  version = "1.93.0"
  hashes = [
    "h1:TPRuokXHSn35kVdANyiFjQr6TSnkc37jbn27fbnpBNk=",
    "zh:3f92d31912953adfc7d39f91e77b80f8fce24aece8f1ea467945ba42c6244414",
    "zh:7ed32a0248371299037ce42e06a710851c97fb982ecfb9e864ede0b33ee2ecfc",
    "zh:90aff055d44a17ee1c0c27939b988eb46dc1d3209131c618fd5437e4eea90442",
    "zh:92ed81ed846001fe77f1c95babbf61ecf3d301583ae42d558fe735ab74c49d0f",
    "zh:a883e0058180482af1eae1a6947357f3140619cada9764ae634273365b03b503",
    "zh:bb75abfdb3d0daed2a1553eaa541b76c6059c4f6b7c7d940b68584ddcf0ba461",
    "zh:c8b96c63443a5d410375060bb1c2f67beb8d17a4cb437601000045e119157653",
    "zh:cbe4faed9b832920e07153af1923490c12de64c3b3c87661bdc90086bb853dd9",
    "zh:e3cc6737e7a44197448035bfc5f6801d225a5799c6cef8b943b27cdce4cff20b",
    "zh:e695b0788b98df6cbc65d99cbe9105bdb3c8eb6b2d057a8240e6faad8e1b49b7",
  ]
}

#version 1.10 test
provider "alicloud" {
}
data "alicloud_images" "images_ds" {
  owners     = "system"
  name_regex = "^centos_7"
}

module "ecs_cluster" {
  source  = "alibaba/ecs-instance/alicloud"
  profile = "default"
  version = "~> 2.0"
  region  = "ap-southeast-1"
  

  number_of_instances = 5

  name                        = "my-ecs-cluster"
  use_num_suffix              = true
  image_id = "${data.alicloud_images.images_ds.images.0.id}"
  instance_type               = "ecs.n1.tiny"
  vswitch_id                  = "vsw-t4nbv1m986kf1hcllvu4p"
  security_group_ids          = ["sg-t4nbz24rtrbf28xwcmz0"]
  associate_public_ip_address = true
  internet_max_bandwidth_out  = 10

  key_name = "uc-linux-paul-kp1"

  system_disk_category = "cloud_ssd"
  system_disk_size     = 50

  tags = {
    Created      = "Terraform"
    Environment = "dev"
  }
}